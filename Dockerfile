ARG UPSTREAM_DOCKER_TAG=oss
FROM sonatype/nexus:${UPSTREAM_DOCKER_TAG}

USER root
RUN yum install -y rsync
RUN curl -o cern-root-ca.crt "https://cafiles.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt" && \
    curl -o cern-ca.crt "https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority.crt" && \
    curl -o cern-ca1.crt "https://cafiles.cern.ch/cafiles/certificates/CERN%20Certification%20Authority(1).crt" && \
    curl -o cern-grid-ca.crt "https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority(1).crt" && \
    /usr/lib/jvm/jre/bin/keytool -noprompt -import -file cern-root-ca.crt -storepass changeit -keystore /usr/lib/jvm/jre/lib/security/cacerts -alias cern-root && \
    /usr/lib/jvm/jre/bin/keytool -noprompt -import -file cern-ca.crt -storepass changeit -keystore /usr/lib/jvm/jre/lib/security/cacerts -alias cern-ca && \
    /usr/lib/jvm/jre/bin/keytool -noprompt -import -file cern-ca1.crt -storepass changeit -keystore /usr/lib/jvm/jre/lib/security/cacerts -alias cern-ca1 && \
    /usr/lib/jvm/jre/bin/keytool -noprompt -import -file cern-grid-ca.crt -storepass changeit -keystore /usr/lib/jvm/jre/lib/security/cacerts -alias cern-grid-ca && \
    mv *.crt /etc/pki/tls/certs

# Fall back to plain user
USER nexus



