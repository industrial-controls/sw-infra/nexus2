# Sonatype Nexus 2 deployment

A Nexus 2 server image for the CERN environment.

![Nexus Pro](documentation/nexus-pro.png)

# How to deploy

## Pre-requisite : How to setup Openshift for automated deployment

An Openshift service account can be used to deploy and configure the application. The service account is limited to a given Openshift project (=namespace).
Use the following commands to register a service account - for instance, we use an Openshift project called "test-nexus2-project" and a service account "nexus2-deployer".

```bash
oc create serviceaccount nexus2-deployer
oc policy add-role-to-user admin system:serviceaccount:test-nexus2-project:nexus2-deployer
oc serviceaccounts get-token nexus2-deployer -n test-nexus2-project
```
This should display the authentication token that you can now use to authenticate :

```bash
oc login <YOUR_SERVER> --token=<YOUR_NEW_TOKEN>
```


## Step 1 - Optionally push images to the Gitlab registry

New images are pushed by Gitlab CI automatically. You can also build and push them manually.

```bash
export VERSION=<NEW_VERSION>
docker build -t gitlab-registry.cern.ch/industrial-controls/sw-infra/nexus2:$VERSION .
docker push gitlab-registry.cern.ch/industrial-controls/sw-infra/nexus2:$VERSION

```

## Step 2 - Process OpenShift templates

To bootstrap deployment, you must apply the templates once (to create the required persistent volume claims a.k.a. PVCs). After that, the CI pipeline will automatically apply any changes you make to the templates (of course, the PVCs are left untouched to preserve persistent data).

For example :
```bash
oc process -p NAMESPACE=test-nexus -p IMAGE_VERSION=oss-staging -f deployment/templates/deployment.yml --local=true | oc apply -f - --server=https://api.paas.okd.cern.ch --token=$OPENSHIFT_TOKEN
oc process -p NAMESPACE=test-nexus -f deployment/templates/service.yml --local=true | oc apply -f - --server=https://api.paas.okd.cern.ch --token=$OPENSHIFT_TOKEN
```

# How to develop locally

```bash
docker run -ti --rm --net=host  -v /tmp/sonatype_work:/sonatype-work:z gitlab-registry.cern.ch/industrial-controls/sw-infra/nexus2:oss-staging
```


# How to synchronize file systems

The Openshift deployment ships a small RSYNC pod sidecar service that allows to access the persistent storage (/sonatype-work) even when the Nexus service is shut down

```bash
oc get pods
oc rsync <LOCAL_PATH> rsync-pod:/sonatype-work
```

Note : the Nexus official image expects all files to be located directly under ```/sonatype-work```. It should contains the usual Nexus 2 folder hierarchies (```storage```, ```conf``` )....

Please note that the ```/sonatype-work/conf/nexus.xml``` file contains the base URL of your deployment. Make sure you update it when deploying the service at a new address.
